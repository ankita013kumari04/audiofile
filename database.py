import mysql.connector


db = mysql.connector.connect(
    host = "localhost",
    user = "root",
    passwd = "anu123",
    database = "audiofile",
)

mycursor = db.cursor()
mycursor.execute("CREATE TABLE song (id INT AUTO_INCREMENT PRIMARY KEY, song_name VARCHAR(100) NOT NULL , song_duration INT NOT NULL CHECK (song_duration > 0), song_uploadedtime datetime default now())")
mycursor.execute("CREATE TABLE podcast (id INT AUTO_INCREMENT PRIMARY KEY, podcast_name VARCHAR(100) NOT NULL , podcast_duration INT NOT NULL CHECK (podcast_duration > 0), podcast_uploadedtime datetime default now() , host VARCHAR(100) NOT NULL , participants VARCHAR(100))")
mycursor.execute("CREATE TABLE audiobook (id INT AUTO_INCREMENT PRIMARY KEY, audiobook_title VARCHAR(100) NOT NULL ,title_author VARCHAR(100) NOT NULL , audiobook_narrator VARCHAR(100) NOT NULL , audiobook_duration INT NOT NULL CHECK (audiobook_duration > 0), audiobook_uploadedtime datetime default now())")

