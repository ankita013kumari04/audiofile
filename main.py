from flask import Flask,request,redirect,url_for,jsonify
from flask_mysqldb import MySQL

app = Flask(__name__)

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'anu123'
app.config['MYSQL_DB'] = 'audiofile'

mysql = MySQL(app)


@app.route('/create', methods = ["POST"])
def create_api():
    if request.method =="POST":
        data = request.json
        audioFileType = data['audioFileType']
        if audioFileType is None:
            return "The request is invalid: 400 bad request", 400
        cur = mysql.connection.cursor()
        if audioFileType == "song":
            song_name=data["audioFileMetadata"]["songName"]
            song_duration=data["audioFileMetadata"]["songDuration"]
            cur.execute("INSERT INTO song(song_name,song_duration) VALUES (%s,%s)",(song_name,song_duration))
            mysql.connection.commit()
            return jsonify({"song_name":song_name,"song_duration":song_duration})
        elif audioFileType == "podcast":
            podcast_name=data["audioFileMetadata"]["podcastName"]
            podcast_duration=data["audioFileMetadata"]["podcastDuration"]
            host=data["audioFileMetadata"]["host"]
            if 'participants' in data["audioFileMetadata"]:
                participants = data["audioFileMetadata"]['participants']
                if (len(participants)>10):
                    return "Enter only 10 participants"
                participant_list=','.join(participants)
                cur.execute("INSERT INTO podcast(podcast_name,podcast_duration, host,participants) VALUES (%s,%s,%s ,%s)",(podcast_name,podcast_duration,host,participant_list))
                mysql.connection.commit()
                return jsonify({"podcast_name":podcast_name,"podcast_duration":podcast_duration,"host":host,"participants":participant_list})
            else:
                cur.execute("INSERT INTO podcast(podcast_name,podcast_duration, host) VALUES (%s,%s,%s)",(podcast_name,podcast_name,host))
                mysql.connection.commit()
                return jsonify({"podcast_name":podcast_name,"podcast_duration":podcast_duration,"host":host})
        elif audioFileType == "audiobook":
            audiobook_title=data["audioFileMetadata"]["audiobookTitle"]
            audiobook_duration=data["audioFileMetadata"]["audiobookDuration"]
            title_author = data["audioFileMetadata"]['titleAuthor']
            audiobook_narrator = data["audioFileMetadata"]['audiobookNarrator']
            cur.execute("INSERT INTO audiobook(audiobook_title,audiobook_duration, title_author,audiobook_narrator) VALUES (%s,%s,%s ,%s)",(audiobook_title,audiobook_duration,title_author,audiobook_narrator))
            mysql.connection.commit()
            return jsonify({"audiobook_title":audiobook_title,"audiobook_duration":audiobook_duration,"title_author":title_author,"audiobook_narrator":audiobook_narrator})
        else:
            return "please enter correct audiotype file"

@app.route("/delete/<string:audiofiletype>/<int:audiofileid>", methods = ["POST"])
def delete(audiofiletype,audiofileid):
    cur = mysql.connection.cursor()
    checkid = "SELECT * FROM {} WHERE id= {}".format(audiofiletype,audiofileid)
    if not cur.execute(checkid) :
        return "Please enter correct id"
    query ="DELETE FROM {} WHERE id= {}".format(audiofiletype,audiofileid)
    cur.execute(query)
    mysql.connection.commit()
    return "data deleted successfully"

@app.route("/update/<string:audiofiletype>/<int:audiofileid>", methods = ["POST"])
def update(audiofiletype,audiofileid):
    if request.method =="POST":
        cur = mysql.connection.cursor()  
        checkid = "SELECT * FROM {} WHERE id= {}".format(audiofiletype,audiofileid)
        if not cur.execute(checkid) :
            return "Please enter correct id"      
        data = request.json    
        cur = mysql.connection.cursor()
        if audiofiletype == "song":
            song_name=data["audioFileMetadata"]["songName"]
            song_duration=data["audioFileMetadata"]["songDuration"]
            cur.execute("UPDATE song SET song_name = %s ,song_duration = %s , song_uploadedtime = now() WHERE id = %s",(song_name,song_duration,audiofileid))
            mysql.connection.commit()
            return jsonify({"song_name":song_name,"song_duration":song_duration})
        elif audiofiletype == "podcast":
            podcast_name=data["audioFileMetadata"]["podcastName"]
            podcast_duration=data["audioFileMetadata"]["podcastDuration"]
            host=data["audioFileMetadata"]["host"]
            if 'participants' in data["audioFileMetadata"]:
                participants = data["audioFileMetadata"]['participants']
                if (len(participants)>10):
                    return "Enter only 10 participants"
                participant_list=','.join(participants)
                cur.execute("UPDATE podcast SET podcast_name = %s ,podcast_duration = %s , host = %s ,participants = %s, podcast_uploadedtime = now()  WHERE id = %s",(podcast_name,podcast_duration,host,participant_list,audiofileid))
                mysql.connection.commit()
                return jsonify({"podcast_name":podcast_name,"podcast_duration":podcast_duration,"host":host,"participants":participant_list})

            cur.execute("UPDATE podcast SET podcast_name = %s ,podcast_duration = %s , host = %s , podcast_uploadedtime = now()  WHERE id = %s",(podcast_name,podcast_duration,host,audiofileid))
            mysql.connection.commit()
            return jsonify({"podcast_name":podcast_name,"podcast_duration":podcast_duration,"host":host})
        elif audiofiletype == "audiobook":
            audiobook_title=data["audioFileMetadata"]["audiobookTitle"]
            audiobook_duration=data["audioFileMetadata"]["audiobookDuration"]
            title_author = data["audioFileMetadata"]['titleAuthor']
            audiobook_narrator = data["audioFileMetadata"]['audiobookNarrator']
            cur.execute("UPDATE audiobook SET audiobook_title = %s ,audiobook_duration = %s , title_author = %s ,audiobook_narrator= %s, audiobook_uploadedtime = now()  WHERE id = %s",(audiobook_title,audiobook_duration,title_author,audiobook_narrator,audiofileid))
            mysql.connection.commit()
            return jsonify({"audiobook_title":audiobook_title,"audiobook_duration":audiobook_duration,"title_author":title_author,"audiobook_narrator":audiobook_narrator})
        else:
            return "Please enter correct audiofiletype"
        




@app.route("/read/<string:audiofiletype>/<int:audiofileid>", methods = ["GET"])
def read(audiofiletype,audiofileid):
    cur = mysql.connection.cursor()
    checkid = "SELECT * FROM {} WHERE id= {}".format(audiofiletype,audiofileid)
    if not cur.execute(checkid) :
        return "Please enter correct id"
    else:
        data=[]
        columns = [column[0] for column in cur.description]
        for row in cur.fetchall():        
            data.append(dict(zip(columns, row)))
        cur.close()
        return jsonify({"data": data})

@app.route("/read/<string:audiofiletype>", methods = ["GET"])
def readall(audiofiletype):
    
    cur = mysql.connection.cursor()
    checkid = "SELECT * FROM {}".format(audiofiletype)
    if not cur.execute(checkid) :
        return "Please enter correct filetype"
    else:
        data=[]
        columns = [column[0] for column in cur.description]
        for row in cur.fetchall():        
            data.append(dict(zip(columns, row)))
        cur.close()
        return jsonify({"data": data})


if __name__ == "__main__":
    app.run(debug = True)
